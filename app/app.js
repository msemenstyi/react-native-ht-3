import { TabNavigator } from 'react-navigation';

import HistoryScreen from './history/history.component.js';
import CalculationsScreen from './calculations/calculations.component.js';

const secondApp = TabNavigator({
  Calculations: {
    screen: CalculationsScreen,
  },
  History: {
    screen: HistoryScreen,
  }
}, {
  tabBarOptions: {
    activeTintColor: '#e91e63',
    showIcon: true,
    iconStyle: {
    	width: 26,
    	height: 26
    }
  }
});

export default secondApp;