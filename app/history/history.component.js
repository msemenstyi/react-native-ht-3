import React, { Component } from 'react';

import {
  StyleSheet,
  Button,
  Image,
  View,
  ListView,
  AsyncStorage,
  Text
} from 'react-native';

class HistoryScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'History',
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require('./history.png')}
        style={[styles.icon, {tintColor: tintColor}]}
      />
    ),
  };

  constructor(){
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
     dataSource: ds.cloneWithRows([{number: 5}])
    };

    // setInterval(() => {
    //   this.updateData();
    // }, 1000);
  }

  async updateData(){
    try {
      const dataString = await AsyncStorage.getItem('results');
      const data = JSON.parse(dataString);
      this.setState((state) => {
        return {
          dataSource: state.dataSource.cloneWithRows(data)
        };
      });
    } catch (e) {
      throw e;
    }
  }

  render() {
    console.log('!!!!!!!!!did');
    
    return (
      <View style={styles.container}>
        <ListView 
          dataSource={this.state.dataSource}
          renderRow={(rowData) => 
            <View style={styles.row}>
              <Text>{rowData.number}</Text>
              <Text style={styles.result}>{rowData.result}</Text>
            </View>}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 20
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: 10,
    backgroundColor: '#F6F6F6',
  },
  result: {
    paddingLeft: 20
  },
  icon: {
    width: 26,
    height: 26,
  },
});

export default HistoryScreen;