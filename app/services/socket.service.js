import socketio from 'socket.io-client'
import EventEmitter from 'EventEmitter';

import { AsyncStorage } from 'react-native';

class SocketService extends EventEmitter{

    constructor(){
        super();
        this.connect();
    }

    async connect(){
      this.socket = socketio.connect('http://localhost:7000');

      this.socket.on('result', async (data) => {
        this.emit('result', data);

        try {
          const messagesString = await AsyncStorage.getItem('results');
          let messages = [];
          if (messagesString){
              messages = JSON.parse(messagesString);
          } 
          messages.push(data);
          await AsyncStorage.setItem('results', JSON.stringify(messages));
        } catch (error) {
          console.log(error);
        }
      });
    }

    getData(){
      this.socket.emit('get-factorial');
    }
}

const socketService = new SocketService();

export default socketService;