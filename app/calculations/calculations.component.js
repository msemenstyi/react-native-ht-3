import React, { Component } from 'react';

import {
  StyleSheet,
  Button,
  Image,
  View,
  Text,
  Animated
} from 'react-native';

import socketService from '../services/socket.service.js';

class CalculationScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Calculations',
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require('./calculations.png')}
        style={styles.icon}
        color={tintColor}
      />
    ),
  };

  constructor(){
    super();
    this.state = {
      top: new Animated.Value(0),
      takeNumber: 0
    };
    socketService.addListener('result', (data) => {
      this.setState(data);
      this.animate();
      this.setState((state) => {
        return {
          takeNumber: state.takeNumber + 1
        };
      });
    });

  }

  animate(){
    if (this.state.takeNumber % 2 === 0){
      Animated.spring(
        this.state.top,
        {
          toValue: 100
        }
      ).start();   
    } else {
      Animated.spring(
        this.state.top,
        {
          toValue: 0
        }
      ).start();         
    }
    
  }

  getData(){
    socketService.getData();
  }

  render() {
    return (
      <View>
        <Button
          onPress={this.getData.bind(this)}
          title="Calculate"
          style={styles.calculateButton}
        />

        { this.state.number ? 
          (<View>
            <Animated.Text style={[styles.number, {top: this.state.top}]}>{this.state.number}</Animated.Text>
            <Animated.Text style={[styles.number, {top: this.state.top}]}>{this.state.result}</Animated.Text>
        </View>) 
          : null
        }

      </View>
    );
  }
}

const styles = StyleSheet.create({
  icon: {
    width: 26,
    height: 26
  },
  calculateButton: {
    marginTop: 200
  },
  number: {
    color: 'green'
  }
});

export default CalculationScreen;