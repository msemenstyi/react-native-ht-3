const http = require('http');
const fs = require('fs');
const socketio = require('socket.io');

function factorial(num){
	var rval = 1;
	for (var i = 2; i <= num; i++)
		rval = rval * i;
	return rval;
}

const app = http.createServer((req, res) => {
	if (req.url === '/'){
		fs.createReadStream(__dirname + '/test-client.html').pipe(res);
	} else if (req.url.indexOf('socket') !== -1){
		fs.createReadStream(__dirname + '/../node_modules/socket.io-client/dist/socket.io.js').pipe(res);
	}
});

var io = socketio(app);

app.listen(7000);

io.on('connection', function (socket) {
	console.log('wow');
  socket.on('get-factorial', function (data) {
  	console.log('request!');
  	const number = Math.round(Math.random() * 50);
	const result = factorial(number);
    socket.emit('result', {result, number});
  });
});